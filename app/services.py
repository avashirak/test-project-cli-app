import csv
from datetime import datetime

import pandas as pd
from vk_api.exceptions import AuthError
from .utils import format_bdate, format_gender, extract_title

''' Класс для обьекта ВК Приложения в dev режиме, 
    в данный момент он прямо не действует на приложение,
    создан для будущего дополнения функционала '''


class VKServices:
    def __init__(self, vk_app_id: str, vk_app_secret_key: str):
        self._vk_app_id = vk_app_id
        self._vk_app_secret_key = vk_app_secret_key

    @staticmethod
    def get_friend_list(vk, user_id: str):
        """ Статический метод для получения list из списка друзей пользователя по user_id """
        try:
            friend_list = vk.friends.get(user_id=user_id, fields=["first_name", "last_name",
                                                                  "country", "city", "bdate", "sex"])
            return friend_list["items"]
        except AuthError as e:
            print(f"Auth error: {e}")
            return []

    @staticmethod
    def save_friends_list_to_csv(friend_list, file_name: str, page_size: int = 10) -> None:
        """ Статический метод для сохранения списка друзей в CSV файл """
        rows = []
        for i in range(0, len(friend_list), page_size):
            try:
                page = friend_list[i:i + page_size]
                rows += [[friend["id"], friend.get("first_name", "None"), friend.get("last_name", "None"),
                          extract_title(friend.get("country", "None")), extract_title(friend.get("city", "None")),
                          format_bdate(friend.get("bdate", "None")), format_gender(friend.get("sex", "None"))]
                         for friend in page]

                # Сохраняем данные в csv-файл
                df = pd.DataFrame(rows, columns=["id", "first_name", "last_name",
                                                 "country", "city", "bdate", "sex"])
                df = df.sort_values(by="first_name")
                df.to_csv(file_name, index=False, )
            except UnicodeEncodeError:
                continue
