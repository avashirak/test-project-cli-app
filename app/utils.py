import json
from datetime import datetime


'''Здесь функции которые нам могут понадобиться в различных ситуациях,
   что бы не было лишнего кода в основных функциях'''


def auth_handler():
    """ Авторизация двухфакторной аутентификации """
    code = input("Enter authentication code: ")
    remember_device = False
    return code, remember_device


def format_bdate(bdate):
    """ Используем чтобы форматировать даты в ISO формат """
    if bdate is None:
        return 'None'
    bdate_parts = bdate.split('.')
    if len(bdate_parts) == 3:
        try:
            bdate_obj = datetime.strptime(bdate, '%d.%m.%Y')
        except ValueError:
            bdate_obj = None
    elif len(bdate_parts) == 2:
        try:
            bdate_obj = datetime.strptime(bdate, '%d.%m')
        except ValueError:
            bdate_obj = None
    else:
        bdate_obj = None
    if bdate_obj is None:
        return 'None'
    return bdate_obj.isoformat()


def format_gender(sex):
    """ В API гендер передается как 1 или 2(Ж, М) """
    if sex is None:
        return 'None'
    elif sex == 1:
        return 'Female'
    elif sex == 2:
        return 'Male'


def extract_title(dct):
    """ Без этой функции в csv файл страна и город передается в виде словаря """
    if dct == 'None':
        return 'None'
    return dct['title']
