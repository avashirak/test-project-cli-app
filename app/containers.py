from dependency_injector import containers, providers

from .services import VKServices


class Container(containers.DeclarativeContainer):
    config = providers.Configuration(yaml_files=["config.yml"])

    VKService = providers.Factory(
        VKServices,
        vk_app_id=config.vk.app_id,
        vk_app_secret_key=config.vk.secret_key,
    )
