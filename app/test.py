import pytest
import vk_api
from vk_api.exceptions import AuthError

from .services import VKServices
from .containers import Container


class TestVkFriendListService:
    def setup_method(self):
        self.vk_app_id = Container.config.vk.app_id
        self.vk_app_secret = Container.config.vk.secret_key
        self.service = VKServices(self.vk_app_id, self.vk_app_secret)

    def test_save_friends_list_to_csv(self, tmp_path):
        friend_list = [{"id": 1, "first_name": "John", "last_name": "Doe"},
                       {"id": 2, "first_name": "Jane", "last_name": "Doe"},
                       {"id": 3, "first_name": "Bob", "last_name": "Smith"}]
        file_path = tmp_path / "test.csv"

        self.service.save_friends_list_to_csv(friend_list, str(file_path))

        assert file_path.exists()
        assert file_path.read_text() == "id,first_name,last_name\n1,John,Doe\n2,Jane,Doe\n3,Bob,Smith\n"
