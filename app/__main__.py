import getpass

import vk_api
from vk_api.exceptions import AuthError, Captcha
from dependency_injector.wiring import inject, Provide

from .utils import auth_handler
from .containers import Container
from .services import VKServices


def login():
    username = input("Enter your VK login(phone number): ")
    password = getpass.getpass(prompt="Enter your VK password: ")
    return username, password


@inject
def main(service: VKServices = Provide[Container.VKService]) -> None:
    username, password = login()

    try:
        '''Залогинились, или если данные неверные то принтим пользователю что случилось, 
           а если есть двухфакторная аутентификация то просим код'''
        vk_session = vk_api.VkApi(
            username,
            password,
        )

        vk_session.auth_handler()

    except AuthError as e:
        if str(e) == 'No handler for two-factor authentication':
            vk_session = vk_api.VkApi(
                username,
                password,
                auth_handler=auth_handler
            )
            vk_session.auth()

        else:
            print("Auth error: Login or Password incorrect")
            return
    except Captcha:
        print("There were too many login requests from your account,\n "
              "please log in to your account from any device and try again \n")
        return

    vk = vk_session.get_api()

    while True:
        target_user_id = input("Enter VK user id to get friend list or write 'self': ")
        # Надо написать ID пользователя ВК или написать self чтобы получить свои данные
        try:
            if target_user_id in ['self', 'Self']:
                target_user_id = vk.users.get()[0]['id']
                print(target_user_id)
                friend_list = service.get_friend_list(vk=vk, user_id=target_user_id)
                service.save_friends_list_to_csv(friend_list, "friend_list.csv")

            else:
                friend_list = service.get_friend_list(vk=vk, user_id=target_user_id)
                service.save_friends_list_to_csv(friend_list, "friend_list.csv")
        except PermissionError:
            print('You may be not have closed the CSV file, close it and try again')
            continue

        user = vk.users.get(user_ids=target_user_id)[0]

        # Если у пользователя пустой список друзей то принтим это
        if not friend_list:
            print(f"No friends found for user {user['first_name']} {user['last_name']}")
            return

        # Успешно создаётся csv файл, создается через несколько секунд
        print(f"Friend list of {user['first_name']} {user['last_name']} saved to friend_list.csv")

        answer = input("Would you like to get information about another user? (yes/no): ")
        if answer.lower() != "yes":
            break


if __name__ == "__main__":
    container = Container()
    container.config.from_yaml("config.yml")
    container.wire(modules=[__name__])

    main()
