Шаги для использования приложения:

```
-1.Чтобы клонировать проект на свое устройство напишите
 git clone https://gitlab.com/avashirak/test-project-cli-app.git

0.У вас на устройстве должен быть установлен Python

Создание виртуального окружения
1. python -m venv venv

Вход в venv
2. source venv/bin/activate or venv\Scripts\activate (on Windows)

Скачивание нужных библиотек
3. pip install -r requirements.txt

Для начала работы приложения
4. python -m app
Enter your VK login(phone number): Вводите свой номер телефона от аккаунта ВК
Enter your VK password: Вводите пароль

Если у вас на аккаунте стоит двухфакторная аутентификация то у вас попросят код который придет на ваше устройство
4 последние цифры если вам начали звонить
Enter authentication code:
Но если логинится слишком часто, то код может перестать приходить в неопределенное время
(советую именно сбрасывать каждый звонок от ВК)
Можете ввести статичные резервные коды от вашего акканта

Enter VK user id to get friend list or write 'self': Вводите id(например мой 311840436) нужного пользователя
                                                     или self для получения 
                                                     списка друзей со своего аккаунта
После этого у вас появится файл friend_list.csv где будет список друзей 
```


# Диаграмма приложения
https://imgur.com/a/NAd9onn

# Список API Endpoints
Авторизация пользователя: https://oauth.vk.com/authorize

Получение токена доступа: https://oauth.vk.com/access_token

Получение данных пользователя: https://api.vk.com/method/users.get

Получение списка друзей пользователя: https://api.vk.com/method/friends.get

# Пример итогового CSV файла:
```
id,first_name,last_name,country,city,bdate,sex
188771376,Аваширак,Доширак,None,None,None,Male
200223858,Артур,Мухитов,Казахстан,Атырау,2001-03-07T00:00:00,Male
192171474,Владимир,Леонов,Казахстан,Атырау,None,Male
194034688,Владимир,Гриневич,Казахстан,Атырау,1999-07-20T00:00:00,Male
180770911,Ярослав,Кучер,Казахстан,Атырау,2002-01-14T00:00:00,Male
```

# Бонусы 
Бонусы
1. Реализована пагинация. Друзей может быть много, они могут не влезть на одну страницу, и
тогда отчет будет неполным;
2. Учет большого количества данных. Если друзей слишком много, мы можем не влезть в
ограничения по опративной памяти. Надо стараться расходовать память с умом; 
3. Расширяемость кода. Необходимо подумать о том, в какую сторону код может расширяться.
Что, если отчет потребуется отдать в файле формата YAML? На сколько болезнены будут такие
изменения? 

-> Расширяемость кода хорошая благодаря фреймворку dependency_injector, 
YML файл будет не сложно сделать как формат вывода
4. Код хорошо читается. При чтении кода быстро становится понятно, что тут происходит;

-> Постарался расписать комментарии на возможно непонятные моменты
5. У кода есть организация. При изучении репозитория должно быть понятно, о чем этот
репозиторий, глядя на структуру файлов и каталогов;

-> Написал структуру кода в несколько файлов с подходящими и понятными названиями
6. В файлике readme кроме инструкции лежит описание того, как работает скрипт, а так же
описание API энд-поинтов, которые задействованы в скрипте;
7. Код покрыт тестами. Что будет, если придет другой человек и изменит код так, что он
перестанет работать? Как ему сразу понять, что он изменил лишнего?
8. Пользователю должно быть удобно. Если считаете интерфейс входных данных неудачным,
можно предложить свой, но нужно аргументировать;
9. В коде есть комментарии, там, где они требуются. Например, для узних мест, костылей и
прочего;
10. В системе присутстуют логи, по которым можно отследить состояние системы во время
выполнения;
